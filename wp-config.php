<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

define('WP_CACHE', false); // Added by WP Rocket

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', "nitdev_wp830");

/** MySQL database username */
define('DB_USER', "nitdev_wp830");

/** MySQL database password */
define('DB_PASSWORD', "pF3()d23lS");

/** MySQL hostname */
define('DB_HOST', "localhost:3306");

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dcl7utumvgttl1go7hriomayz4pb6q2lio6f6hv1qopjevxjnjhvhe8ntsqo97jp');
define('SECURE_AUTH_KEY',  '5evqvjir4ylexcbgrisfh2htqoshatdyjwd0k908qjeuwxvpp2hyngclzgtprnbu');
define('LOGGED_IN_KEY',    'iihjo37rhhrfpqllv8mwzr3zxtsettqjf9hzsksl7redsoypvb4rpfgnjk76w1lg');
define('NONCE_KEY',        'hlrogdy6lyt2mpj7qphng4mzo48dfetevqdvwx7wdbngrlxfb36kakg7q0rhpyh5');
define('AUTH_SALT',        'q4jesbbeuxtrmzoficlqdadx0p4x1zib7rjdecgywwt9jzhi6vhs8qg2tukbnuvq');
define('SECURE_AUTH_SALT', 'dosit1rcne2osxgllcruhfce9anj5s5nh2mbmczseniqn3vedmav57gihxsyxkpi');
define('LOGGED_IN_SALT',   'jwwg6hyhqz6iem2m5u8oy5x3zrrw4z8bh8t2pbskopywsnciqwg4qtyjmovwvb9a');
define('NONCE_SALT',       'tivfjfzrwqvfcek8threymdotids4thufahffdym2q7r7u6mggbtgapnncndgqb2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = "wpao_";

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
