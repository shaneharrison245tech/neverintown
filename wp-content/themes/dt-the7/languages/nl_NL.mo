��    Y      �     �      �  
   �  	   �     �  	   �     �     �     �       
        #     1     G     L     T     \     d     p     �     �  	   �     �     �     �  	   �  
   �     �     �     	  
   	  	   	     	     )	     .	     =	     D	     J	     R	     [	     d	     u	  \   �	     �	     �	  	   
     
     
     -
     6
     >
     R
  )   `
     �
     �
     �
     �
     �
     �
       $   $     I     \     q  &   �     �     �     �     �     �               3  #   J     n  )   �     �     �     �     �                *     @     [     m     �     �     �     �  t  �  
   K  	   V     `     u     �     �     �     �     �     �     �                          %     ;     S     `     {     �     �     �  
   �     �     �     �     �  
   �  
   �                 	   2     <     B  
   J  	   U     _     r  \   �  	   �     �     �                    '     /     G  '   U     }     �     �     �  	   �     �  	   �     �     �     �     �     �       	   !     +  	   1     ;     @     H     U     ^  	   v     �     �     �     �     �     �  	   �     �     �     �     �     �          (  	   7     8       C   H          M   +   R      <   (                  :       S   Q   $      &   B   >      -   P               1           7           3   L                          U   )   G          9       E   ,   /      @   !                       .   K   5              F      D      I                   	   T   J       ?   2   *   0          =   A               X       ;      %   6          N       O          #                 V              "          
   4   Y      '   W    % Comments 1 Comment Apply Coupon Archives: Article author %s Author Archives: %s Available on backorder By %s Categories Category "%s" Category Archives: %s City Comment Company Country Coupon code Daily Archives: %s Details Entries tagged with "%s" Error 404 Find us on: Leave a comment Link Load more Loading... Message Monthly Archives: %s Name Name &#42; Name&#42; Nothing Found Page Page not found Pages: Price Product Quantity Required Results for "%s" Search Results for: %s Sorry, but nothing matched your search terms. Please try again with some different keywords. Submit Tag Archives: %s Telephone Total Type and hit enter &hellip; View all Website Yearly Archives: %s You are here: Your email address will not be published. backendExport/Import Options backendSidebar backendTheme Options backendname backend metaboxLazy loading backend metaboxNone backend metaboxOrder: backend metaboxheader &amp; top bar backend metaboxno backend metaboxnone by Dream-Theme comments title1 Comment %1$s Comments details buttonDetails submit team linkE-mail theme-optionsArchives theme-optionsBranding theme-optionsButtons theme-optionsCategories theme-optionsComments theme-optionsExport/Import Options theme-optionsFooter theme-optionsImages Styling &amp; Hovers theme-optionsLink theme-optionsNo theme-optionsNone theme-optionsShare Buttons theme-optionsSidebar theme-optionsSkins theme-optionsStripes theme-optionsWidget areas theme-optionsYes widgetContact info widgetCustom menu style 1 widgetSend mail to: widgetSocial links: widgetrequired Project-Id-Version: The7mk2 v5.0.0.b5
Report-Msgid-Bugs-To: https://support.dream-theme.com/
POT-Creation-Date: 2019-03-20 09:52:19+00:00
PO-Revision-Date: 2016-09-23 11:32+0300
Last-Translator: 
Language-Team: Dutch
Language: nl-NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Loco-Target-Locale: nl_NL
X-Poedit-SearchPath-0: .
 % Reacties 1 Reactie Gebruik de waardebon Archieven:  Schrijver van het artikel %s Schrijver Archieven %s Beschikbaar op backorder Door %s Categorieën Categorie \ %s\ Categorie Archieven: %s Stad Reactie Bedrijf Land Code van de waardebon Dagelijkse Archieven %s Lees verder! Ingangen getaged met \ %s\ Foutmelding 404 Vind ons op: Laat een reactie achter Link  Laadt meer Laden… Bericht Maandelijkse Archieven %s Naam Naam &#42; Naam &#42; Niets gevonden Pagina Pagina niet gevonden Pagina's: Prijs Product Kwantiteit Verplicht Resultaten voor %s Resultaten voor: %s Helaas komt niets overeen met je zoekopdracht. Probeer het eens met een andere zoekopdracht. Verzenden Tag Archieven %s Telefoon Totaal Zoeken &hellip; Allemaal Website Jaarlijkse Archieven %s Je bent hier: Je email adres wordt niet gepubliceerd. Importeren & Exporteren Balk aan de zijkant Thema Opties naam Lui laden Geen Volgorde: Bar bovenaan Nee Geen door: Dream-Theme 1 Reactie %1$s Reacties Lees verder! verzenden Email Archieven Merk Knoppen Categorieën Reacties Importeren & Exporteren Onderkant Afbeeldingen Stijlen Link  Nee Geen Deel knoppen Bar aan de zijkant Uiterlijk Strepen Widget locaties Ja Contact informatie Aangepaste menu stijl 1 Verzend email naar: Sociale links: Verplicht 