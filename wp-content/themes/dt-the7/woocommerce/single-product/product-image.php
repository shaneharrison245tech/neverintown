<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

// Post featured image.
// presscore_get_template_part( 'theme', 'single-post/post-featured-image' );




// $columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
// $post_thumbnail_id = $product->get_image_id();
// $wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
// 	'woocommerce-product-gallery',
// 	'woocommerce-product-gallery--' . ( $product->get_image_id() ? 'with-images' : 'without-images' ),
// 	'woocommerce-product-gallery--columns-' . absint( $columns ),
// 	'images',
// ) );
?>
<!-- <div class="<?php // echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
	<figure class="woocommerce-product-gallery__wrapper">
		<?php
		// if ( $product->get_image_id() ) {
		// 	$html = wc_get_gallery_image_html( $post_thumbnail_id, true );
		// } else {
		// 	$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
		// 	$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
		// 	$html .= '</div>';
		// }

		// echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped

		// do_action( 'woocommerce_product_thumbnails' );
		?>
	</figure>
</div> -->


<div class="product-thumbnail">
		<span class="fancy-categories">
		<?php 
			$categ = $product->get_categories();
			echo $categ; ?>
		</span>

		
		<?php 

$thumb_args = array(
	'class'  => 'rollover post-thumbnail-rollover',
	'img_id' => get_post_thumbnail_id(),
	'href'   => get_permalink(),
	'wrap'   => '<img %IMG_CLASS% %SRC% %ALT% %IMG_TITLE% %SIZE% />',
);

$thumb_args = apply_filters( 'dt_post_thumbnail_args', $thumb_args );

dt_get_thumb_img( $thumb_args );

		// presscore_get_template_part( 'theme', 'single-post/post-featured-image' );	
			// presscore_wc_template_loop_product_thumbnail( 'alignnone' ); 
			// if ( !$product->is_in_stock() ) {
			// 	echo '<span class="out-stock-label">'.__('Out Of Stock','the7mk2').'</span>';
			// }
		
			// echo presscore_get_blog_post_fancy_date();
	
			// $thumb_args = array(
			// 	'class'  => 'rollover post-thumbnail-rollover',
			// 	'img_id' => get_post_thumbnail_id(),
			// 	'href'   => get_permalink(),
			// 	'wrap'   => '<a %HREF% %CLASS% %CUSTOM%><img %IMG_CLASS% %SRC% %ALT% %IMG_TITLE% %SIZE% /></a>',
			// );
	
			// $thumb_args = apply_filters( 'dt_post_thumbnail_args', $thumb_args );
	
			// dt_get_thumb_img( $thumb_args );
			?>
			
			

	</div>
<?php
	// Post content.
// echo '<div class="entry-content">';
// the_content();
// wp_link_pages(
// 	array(
// 		'before' => '<div class="page-links">' . __( 'Pages:', 'the7mk2' ),
// 		'after'  => '</div>',
// 	)
// ); 
// echo '</div>';
?>