<?php
/**
 * The7 theme.
 *
 * @since   1.0.0
 *
 * @package The7
 */

defined( 'ABSPATH' ) || exit;

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since 1.0.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1200; /* pixels */
}

/**
 * Initialize theme.
 *
 * @since 1.0.0
 */
require trailingslashit( get_template_directory() ) . 'inc/init.php';


function myprefix_woo_product_sidebar_attributes() {


    // Remove items from summary
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

	// Add items to sidebar
	if ( is_singular( 'product' ) ) { 
		add_action( 'presscore_before_sidebar_widgets', 'woocommerce_template_single_add_to_cart', 10 );

	}

}
add_action( 'wp', 'myprefix_woo_product_sidebar_attributes' ); 



function my_custom_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Custom', 'neverintowntravel' ),
            'id' => 'custom-side-bar',
            'description' => __( 'Custom Sidebar', 'neverintowntravel' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action( 'widgets_init', 'my_custom_sidebar' );

add_shortcode('woocommerce-gallery', 'woo_gallery_func');
function woo_gallery_func() {
    if ( is_singular( 'product' ) ) {
        ob_start();
        wc_get_template('single-product/product-thumbnails.php');
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] );          // Remove the description tab
    unset( $tabs['reviews'] );          // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab
    return $tabs;
} 


/**
 * Redirect users after add to cart.
 */
function my_custom_add_to_cart_redirect( $url ) {
    $url = WC()->cart->get_cart_url();
    // $url = wc_get_checkout_url(); // since WC 2.5.0
    return $url;
}
add_filter( 'woocommerce_add_to_cart_redirect', 'my_custom_add_to_cart_redirect' ); 

add_action( 'wp_head', 'quantity_wp_head' );
function quantity_wp_head() {
if ( is_product() ) {
?>
<style type="text/css">.quantity, .buttons_added { width:0; height:0; display: none; visibility: hidden; }</style>
<?php }
}
 

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

function woocommerce_template_single_excerpt() {
    return;
}

function dot_do_product_desc() {
    global $woocommerce, $post;

    if ( $post->post_content ) : ?>
        <div itemprop="description" class="item-description">
            <?php the_content(); ?>
        </div>
    <?php endif;
}
add_action( 'woocommerce_single_product_summary', 'dot_do_product_desc', 20 );

