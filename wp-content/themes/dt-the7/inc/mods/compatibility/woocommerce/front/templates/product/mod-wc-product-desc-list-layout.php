<?php
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

// get rollover icons
global $product;
$rollover_icons = dt_woocommerce_get_product_preview_icons();
if ( $rollover_icons ) {
	$rollover_icons = '<div class="woo-buttons">' . $rollover_icons . '</div>';
}
?>
<!-- <figure class="woocom-project"> -->

	<div class="post-thumbnail-wrap">
		<span class="fancy-categories">
		<?php 
			$categ = $product->get_categories();
			echo $categ; ?>
		</span>

		
		<?php 

		// presscore_get_template_part( 'theme', 'single-post/post-featured-image' );	
			// presscore_wc_template_loop_product_thumbnail( 'alignnone' ); 
			// if ( !$product->is_in_stock() ) {
			// 	echo '<span class="out-stock-label">'.__('Out Of Stock','the7mk2').'</span>';
			// }
		
			// echo presscore_get_blog_post_fancy_date();
	
			$thumb_args = array(
				'class'  => 'rollover post-thumbnail-rollover',
				'img_id' => get_post_thumbnail_id(),
				'href'   => get_permalink(),
				'wrap'   => '<a %HREF% %CLASS% %CUSTOM%><img %IMG_CLASS% %SRC% %ALT% %IMG_TITLE% %SIZE% /></a>',
			);
	
			$thumb_args = apply_filters( 'dt_post_thumbnail_args', $thumb_args );
	
			dt_get_thumb_img( $thumb_args );
			?>
			
			

	</div>
	<div class="entry-content">

		<?php echo dt_woocommerce_get_product_description(); ?>
		<?php 
			echo $rollover_icons; 
		?>

	</div>
<!-- </figure> -->