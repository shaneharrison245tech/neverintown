jQuery(document).ready(function($) {

    var gf_cloud_myjq = jQuery.noConflict();

    gf_cloud_myjq('.button_redirect').live('click', function() {
        window.location.href = ADMIN_AJAX_URL + "admin.php?page=gf_edit_forms";
    });

    gf_cloud_myjq('.button_redirect_cf7').live('click', function() {
        window.location.href = ADMIN_AJAX_URL + "admin.php?page=wpcf7";
    });

    gf_cloud_myjq('#gf_cloud_yes').click(function() {
        gf_cloud_myjq('#gf_cloud_userdata').delay(100).fadeOut();
        gf_cloud_myjq('#gf_cloud_yes').attr('disabled', false);
        gf_cloud_myjq('#gf_cloud_loginform').slideDown('slow');

    });

    gf_cloud_myjq('#gf_cloud_no, #cUsCf7i_signup_cloud').click(function() {
        gf_cloud_myjq('.advice_notice').delay(100).fadeOut();
        gf_cloud_myjq('#createpostform').delay(100).fadeOut();
        gf_cloud_myjq('#gf_cloud_loginform').delay(100).fadeOut();
        gf_cloud_myjq('#gf_cloud_userdata').slideDown('slow');
    });

    function checkRegexp(o, regexp, n) {
        if (!(regexp.test(o))) {
            return false;
        } else {
            return true;
        }
    }

    function checkURL(url) {
        return /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/.test(url);
    }

    function str_clean(str) {

        str = str.replace("'", " ");
        str = str.replace(",", "");
        str = str.replace("\"", "");
        str = str.replace("/", "");

        return str;
    }

    gf_cloud_myjq('.gf_cloud_LoginUser').click(function() { //LOGIN ALREADY USERS
        var email = gf_cloud_myjq('#login_email').val();
        var pass = gf_cloud_myjq('#user_pass').val();
	var site_code = gf_cloud_myjq('#site_code').val();
        gf_cloud_myjq('.loadingMessage').show();

        
        if (!site_code.length) {
	    gf_cloud_myjq('.advice_notice').html('Site Code is a required field!').slideToggle().delay(2000).fadeOut(2000);
            gf_cloud_myjq('#site_code').focus();
            gf_cloud_myjq('.loadingMessage').fadeOut();
	} else {
           
                //gf_cloud_myjq('.gf_cloud_LoginUser').val('Loading . . .').attr({disabled: 'disabled'});
                gf_cloud_myjq.ajax({type: "POST", dataType:'json', url: ADMIN_AJAX_URL + 'admin-ajax.php', data: {action: 'gf_cloud_loginAlreadyUser', email: email, pass: pass, site_code: site_code},
                    success: function(data) {
                        switch (data.status) {
                            case 1:

                                gf_cloud_myjq('.gf_cloud_LoginUser').val('Success . . .');
                                message = '<p>Welcome!</p>';

                                setTimeout(function(){
                                    //oLoginForm.slideUp().fadeOut();
                                    location.reload();
                                },2500);

                                jQuery('.notice').html(message).show();
                                gf_cloud_myjq('#gf_cloud_loginform').slideUp().fadeOut();

                                // hide the buttons
                                gf_cloud_myjq('#gfcloud_welcome').hide();
                                //location.reload();


                                break;
                         
                            case 3:

                                gf_cloud_myjq('.gf_cloud_LoginUser').val('Error . . .');
                                gf_cloud_myjq('.gf_cloud_Loginform').slideUp().fadeOut();

                                gf_cloud_myjq('.gf_cloud_LoginUser').val('Login').removeAttr('disabled');


                                message = '<p>Unfortunately, we weren’t able to log you into your account.</p>';
                                message += '<p>Please try again.</p>';
                                message += '<p>Error:  <b>' + data.message + '</b></p>';

                                jQuery('.notice').html(message).show();
                                //gf_cloud_myjq('#createpostform').fadeIn();

                                break;

                            default:
                                message = '<p>There has been an application error. <b>' + data + '</b>. Please try again</p>';
                                jQuery('.notice').html(message).show();
                                gf_cloud_myjq('.gf_cloud_LoginUser').val('Login').removeAttr('disabled');
                                break;
                        }

                        gf_cloud_myjq('.loadingMessage').fadeOut();


                    },
                    async: false
                });
            
        }
    });

    function DisableOptions() {
        var arr = [];
        gf_cloud_myjq("#gf_cloud_table select option:selected").each(function() {
            arr.push(gf_cloud_myjq(this).val());
        });

        gf_cloud_myjq("#gf_cloud_table select option").filter(function() {
            return gf_cloud_myjq.inArray(gf_cloud_myjq(this).val(), arr) > -1;
        }).attr("disabled", "disabled");

    }
});  

