<?php
define('PT', 'pulsetracker_');
define('ADMIN_AJAX_URL', get_admin_url());
define( 'PT_PLUGIN', __FILE__ );
define( 'PT_PLUGIN_BASENAME', plugin_basename( PT_PLUGIN ) );
define( 'PT_PLUGIN_NAME', trim( dirname( PT_PLUGIN_BASENAME ), '/' ) );
define( 'PT_PLUGIN_DIR', untrailingslashit( dirname( PT_PLUGIN ) ) );
define( 'PT_PLUGIN_URL', untrailingslashit( plugins_url( '', PT_PLUGIN ) ) );
global $contact_id;

$remoteIp = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
if ($remoteIp == '192.168.1.100' || $remoteIp == '127.0.0.1') {
    define('PT_URL', 'https://pulsetracker.com/');
    define('API_URL',  PT_URL.'api');
} else {
    define('PT_URL', 'https://pulsetracker.com/');
    define('API_URL', PT_URL.'api');
}

//white label functionality
require_once (dirname(__FILE__) . '/whitelabel.php');
//update plugin
require_once (dirname(__FILE__) . '/update.php');
// Ajax controllers
require_once(dirname(__FILE__) . '/controllers/ajax_response.php');
// require CU API
require_once (dirname(__FILE__) . '/includes/cusAPI.class.php');

class PULSETRACKERGravityForm {

    private $API_url = API_URL;

    function hook_javascript() {
        echo $scriptCode = get_option('pt3_gf_tracking_script');
    }

    public function __construct() {

        $this->setUpMenu();
        add_action('wp_head', array($this, 'hook_javascript'), 10, 2);
        add_action('gform_after_submission', array(&$this, 'gfcloud_form_submit'), 10, 2);

        add_action('admin_print_scripts', array($this, 'Load_scripts'), 10, 2);
        add_action('admin_print_styles', array($this, 'Load_styles'), 10, 2);
        add_action('admin_print_styles', array($this, 'my_styles_method'), 10, 2);

        add_action('wp_ajax_save_list', array($this, 'save_list'));

        add_action('wp_ajax_save_list_cf7', array($this, 'save_list_cf7'));

        add_action('wpcf7_before_send_mail', array($this, 'process_contact_form_data'));

        $this->cf7MapFields();
    }

    function process_contact_form_data( $contact_data ){
        
        $submission = WPCF7_Submission :: get_instance() ;
        if ($submission) {
            $form_id = $contact_data->id;
            $formTitle = $contact_data->title;
            
            $pulsetrackerCustomFields = get_option('tft_cf7_form_' . $form_id);
            $postedData = $submission->get_posted_data();
            
            $data = array();
            if ($postedData) {
                foreach ($postedData as $postedField=>$postedValue) {
                    $key = array_search($postedField, $pulsetrackerCustomFields); 
                    if ($key) {
                        $keyNumberPostion = str_replace('cf7_form_source_', '', $key);
                        $destinationField = $pulsetrackerCustomFields['custom_field_destination_'.$keyNumberPostion];

                        $data[$destinationField] = $postedValue;
                    }
                }

            }

            $this->submitToPULSETRACKERCf7($data, $form_id, $formTitle);
        }
    }


    public function save_list_cf7() {
        
        if(!empty($_POST['param']))
        {
            $mappedFields = $_POST['param']+$_POST['param2'];
            $formId = $mappedFields['formId'];
            $title = get_the_title($formId);
            
            update_option('tft_cf7_form_' . $formId, $mappedFields);
            $this->submitListToPULSETRACKER($title, $formId, 'cf7_form');
        }
    }

    public function save_list() {
        $this->submitListToPULSETRACKER($_POST['title'], $_POST['form_id'], $_POST['form_type']);
    }

    public function my_styles_method() {
        $custom_css = "#gf_form_toolbar li a { text-decoration: none !important; }";
        wp_add_inline_style('custom-style', $custom_css);
    }

    public function gfcloud_form_submit($entry, $form) {
        //getting post
        $pulsetrackerCustomFields = get_option('tft_gravity_form_' . $entry['form_id']);
        $data = array('id' => $entry['id'], 'date_created' => $entry['date_created']);
        
        foreach ($form['fields'] as $key => $field) {
            if( !empty($field['inputs']) ) {
                foreach ($field['inputs'] as $key => $_field) {
                    $_field = (object)$_field;
                    if( !isset($_field->isHidden) || $_field->isHidden !== true ) {
                        $fkey = str_replace(".", "_", $_field->id);
                        if(!empty($pulsetrackerCustomFields['custom_field_destination_' . $fkey]))
                            $data[$pulsetrackerCustomFields['custom_field_destination_' . $fkey]] = $entry[(string)$_field->id];    
                    }
                }
            } else {
                $fkey = str_replace(".", "_", $field->id);
                if(!empty($pulsetrackerCustomFields['custom_field_destination_' . $fkey]))
                    $data[$pulsetrackerCustomFields['custom_field_destination_' . $field->id]] = $entry[$fkey];
            }
        }

        $this->submitToPULSETRACKER($data, $form, $entry);
    }

    public function addGFFieldMap( $args = array() ){
        global $pt_data;
        extract($args);
        $returnField = '';
        $customFields = json_decode($object->fetchPULSETRACKERCustomFields());
        foreach ($fields as $field) {

            if( !empty($field['inputs']) ) {
                $returnField .= $this->addGFFieldMap(
                    array(
                            'fields' => $field['inputs'],
                            'object' => $object,
                            'the_data' => $the_data
                        )
                );
            } else {
                if( is_array($field) ) $field = (object)$field;
                if( !isset($field->isHidden) || $field->isHidden !== true ) {

                    $returnField .= '
                    <tr id="row_'.$field->label.'">
                        <td>
                            Gravity Form field:<br>
                            <input type="text" value="'.$field->label.'" readonly name="gravity_form_source_'.$field->id.'">
                        </td>
                        <td>
                            Select '.$pt_data['Name'].' field to associate:<br />
                            <select id="select2" class="select2" name="custom_field_destination_'.$field->id.'">
                                <option value="">-- Unmapped --</option>';
                                // list and select current select value
                                foreach ($customFields as $skey => $svalue) {
                                    $fkey = str_replace(".", "_", $field->id);
                                    if ($skey == $the_data['custom_field_destination_' . $fkey]) {
                                        $returnField .= '<option value="' . $skey . '" selected="selected">' . $svalue . '</option>';
                                    } else {
                                        $returnField .= '<option value="' . $skey . '">' . $svalue . '</option>';
                                    }
                                }
                    $returnField .= '
                            </select>
                        </td>
                    </tr>';
                }
            }
        }
        return $returnField;
    }

    private function submitListToPULSETRACKER($title, $form_id, $form_type='gravityforms') {

        //$title = $form['title'];
        $strCURLOPT = ''; // $this->API_url.'/save/wpdata';

        $Page_url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        $credentials = get_option('gf_cloud_settings_userCredentials');
        $siteCode = trim(get_option('gf_cloud_settings_siteCode'));

        $strCURLOPT .= 'API_Account=' . $credentials['API_Account'];
        $strCURLOPT .= '&API_Key=' . $credentials['API_Key'];
        $strCURLOPT .= '&Site_Code=' . $siteCode;
        $strCURLOPT .= '&Form_title=' . urlencode($title);
        $strCURLOPT .= '&Form_id=' . $form_id;
        $strCURLOPT .= '&Form_type='.$form_type;
        $strCURLOPT .= '&Page_url=' . urlencode($Page_url);
        $strCURLOPT .= '&random=' . time();

//        echo $this->API_url . '/save/wplistdata?'.$strCURLOPT;
        //exit();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->API_url . '/save/wplistdata?' . ($strCURLOPT));

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    private function submitToPULSETRACKER($data, $form, $entry = array()) {
        
        global $contact_id;
        $form_id = $entry['form_id'];
        $title = ( empty($form['title']) ? 'Gravity Form'. $form_id : $form['title'] );

        $credentials = get_option('gf_cloud_settings_userCredentials');
        $siteCode = trim(get_option('gf_cloud_settings_siteCode'));

        $strCURLOPT = ''; // $this->API_url.'/save/wpdata';
        
        $cid = filter_input(INPUT_COOKIE, 'tft_contact_id');
        $sid = filter_input(INPUT_COOKIE, '_s'.$siteCode);

        $Page_url = filter_input(INPUT_SERVER, 'HTTP_HOST') . filter_input(INPUT_SERVER, 'REQUEST_URI');
        
        $strCURLOPT .= 'API_Account=' . $credentials['API_Account'];
        $strCURLOPT .= '&API_Key=' . $credentials['API_Key'];
        $strCURLOPT .= '&Site_Code=' . $siteCode;
        $strCURLOPT .= '&Form_title=' . urlencode($title);
        $strCURLOPT .= '&Form_id=' . $form_id;
        $strCURLOPT .= '&Form_type=gravityforms';
        $strCURLOPT .= trim($strCURLOPT . '&content=' . base64_encode(serialize($data)));
        $strCURLOPT .= '&Page_url=' . urlencode($Page_url);
        $strCURLOPT .= '&Contact_id=' . urlencode($cid);
        $strCURLOPT .= '&Session_id=' . urlencode($sid);
        $strCURLOPT .= '&ip=' . urlencode(filter_input(INPUT_SERVER, 'REMOTE_ADDR'));
        $strCURLOPT .= '&random=' . time();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->API_url . '/save/wpdata?' . ($strCURLOPT));

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        // $output contains the output string
        $output = curl_exec($ch);
        $pt_output = json_decode($output);
        $contact_id = $pt_output->id;
        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }

    private function submitToPULSETRACKERCf7($data, $form_id, $formTitle) {
        
        global $contact_id;
         
        $title = ( empty($formTitle) ? 'Contact Form 7'. $form_id : $formTitle );

        $credentials = get_option('gf_cloud_settings_userCredentials');
        $siteCode = trim(get_option('gf_cloud_settings_siteCode'));

        $strCURLOPT = ''; // $this->API_url.'/save/wpdata';
        
        $cid = filter_input(INPUT_COOKIE, 'tft_contact_id');
        $sid = filter_input(INPUT_COOKIE, '_s'.$siteCode);

        $Page_url = filter_input(INPUT_SERVER, 'HTTP_HOST') . filter_input(INPUT_SERVER, 'REQUEST_URI');
        
        $strCURLOPT .= 'API_Account=' . $credentials['API_Account'];
        $strCURLOPT .= '&API_Key=' . $credentials['API_Key'];
        $strCURLOPT .= '&Site_Code=' . $siteCode;
        $strCURLOPT .= '&Form_title=' . urlencode($title);
        $strCURLOPT .= '&Form_id=' . $form_id;
        $strCURLOPT .= '&Form_type=cf7_form';
        $strCURLOPT .= trim($strCURLOPT . '&content=' . base64_encode(serialize($data)));
        $strCURLOPT .= '&Page_url=' . urlencode($Page_url);
        $strCURLOPT .= '&Contact_id=' . urlencode($cid);
        $strCURLOPT .= '&Session_id=' . urlencode($sid);
        $strCURLOPT .= '&ip=' . urlencode(filter_input(INPUT_SERVER, 'REMOTE_ADDR'));
        $strCURLOPT .= '&random=' . time();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->API_url . '/save/wpdata?' . ($strCURLOPT));

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        // $output contains the output string
        $output = curl_exec($ch);
        $pt_output = json_decode($output);
        $contact_id = $pt_output->id;
        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }


    public function fetchPULSETRACKERCustomFields() {

        $siteCode = get_option('gf_cloud_settings_siteCode');

        $apiUrl = $this->API_url . '/get/wp/custom-fields/' . $siteCode;

        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $apiUrl);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    public function setUpMenu() {

        add_filter('gform_toolbar_menu', array($this, 'get_toolbar_menu_items'), 10, 2);
        add_action('admin_menu', array($this, 'pulstracker_menu'));
    }

    public function pulstracker_menu() {
        global $pt_data;
        $dir = plugin_dir_url(__FILE__);
        add_menu_page($pt_data["Name"], $pt_data["Name"], 'edit_pages', 'gf-integrations', array($this, 'loadSettings'), get_option('pt_plugin_icon', $dir . 'index.png'));
    }

    public function Load_scripts() {
        global $current_screen; // check we are in our CF7 integrations plugin page
        if ($current_screen->id == 'toplevel_page_gf-integrations' || $current_screen->id == 'toplevel_page_wpgf') {
            wp_register_script('my-scripts', PT_PLUGIN_URL . '/assets/js/scripts.js');
            wp_enqueue_script('my-scripts');
        }
    }

    public function Load_styles() {
        global $current_screen; // check we are in our CF7 integrations plugin page
        if ($current_screen->id == 'toplevel_page_gf-integrations' || $current_screen->id == 'toplevel_page_wpgf') {
            wp_register_style('my-styles', PT_PLUGIN_URL . '/assets/css/styles.css');
            wp_enqueue_style('my-styles');
        }
    }

    public function get_toolbar_menu_items($menu_items, $form_id) {
        global $pt_data;
        //---- Form Editor ----
        $edit_capabilities = array('gravityforms_edit_forms');

        $preview_capabilities = array('gravityforms_edit_forms', 'gravityforms_create_form', 'gravityforms_preview_forms');

        $menu_items[PT] = array(
            'label' => __('Map Fields', 'gravityforms'),
            'icon' => '<i class="fa fa-list fa-lg"></i>',
            'title' => __($pt_data["Name"].' Integration this form', 'gravityforms'),
            'url' => '?page=gf-integrations&id=' . $form_id,
            'menu_class' => 'gf_form_toolbar_pulsetracker_integration',
            'link_class' => rgget('page') == 'gf-integrations' ? 'gf_toolbar_active' : '*',
            'capabilities' => $preview_capabilities,
            'priority' => 800,
        );

        return $menu_items;
    }

    public function loadSettings() {
        if (isset($_GET['id'])) {
            include_once'form_fields.php';
            $form_id = RGForms::get('id');
            GFFormPULSETRACKERFields::forms_page($form_id, $this);
        } else {
            require_once'settings_view.php';
        }
    }

    static function getObject() {
        return new PULSETRACKERGravityForm();
    }

    public function cf7MapFields() {
        $panels = null;
        $panels = apply_filters( 'wpcf7_editor_panels', $panels );
        
        add_filter( 'wpcf7_editor_panels', function($panels) {
            $panels['preview-panel'] = array( 
                    'title' => __( 'Map Fields', 'contact-form-7' ),
                    'callback' => array($this, 'wpcf7_editor_panel_preview')
            );


            return $panels;
        }, 10, 1 );     // 10 = priority (default); 1 = nr of arguments
    }
    
    function wpcf7_editor_panel_preview( $post ) {
       
        $preview_code = $post->form_scan_shortcode();
        $formId = $post->id();
        
        $mapFields = get_option('tft_cf7_form_' . $formId);

        $customFields = json_decode($this->fetchPULSETRACKERCustomFields());

        include_once'map_field_cf7.php';
    }

    public function exportFormDataToPT($form_id = 0){
        if(empty($form_id)) return false;
        $mappedFields = get_option('tft_gravity_form_'.$form_id);
        if(in_array('contact--work_email', $mappedFields)){

            $form = GFFormsModel::get_form_meta($form_id);
            if (!isset($form['fields']) || !is_array($form['fields'])) {
                $form['fields'] = array();
            }
            $form = apply_filters('gform_admin_pre_render_' . $form_id, apply_filters('gform_admin_pre_render', $form));
            $this->exportEntries($form, 0, 20);
        }
    }

    public function exportEntries($form, $offset = 0, $page_size = 20){

        $search_criteria = array();
        $sorting         = array();
        $paging          = array( 'offset' => $offset, 'page_size' => $page_size );
        $total_count     = 0;
        $offset          = $offset+$page_size;

        $entries         = GFAPI::get_entries( $form['id'], $search_criteria, $sorting, $paging, $total_count );

        $mappedFields = get_option('tft_gravity_form_'.$form['id']);
        $data = array();
        foreach ($entries as $entry) {
            $_entry = array('id'=>$entry['id'], 'date_created'=>$entry['date_created']);
            foreach ($form['fields'] as $field) {
                if( !empty($field['inputs']) ) {
                    foreach ($field['inputs'] as $key => $_field) {
                        $_field = (object)$_field;
                        if( !isset($_field->isHidden) || $_field->isHidden !== true ) {
                            $fkey = str_replace(".", "_", $_field->id);
                            if(!empty($mappedFields['custom_field_destination_' . $fkey]))
                                $_entry[$mappedFields['custom_field_destination_' . $fkey]] = $entry[$_field->id];
                        }
                    }
                } else {
                    $fkey = str_replace(".", "_", $field->id);
                    if(!empty($mappedFields['custom_field_destination_' . $fkey]))
                        $_entry[$mappedFields['custom_field_destination_' . $fkey]] = $entry[$field->id];
                }
            }
            $data[] = $_entry;
        }
        
        $this->exportEntryToPULSETRACKER($data, $form);
        if( $offset < $total_count ){
            $this->exportEntries($form, $offset, $page_size);
        }
    }

    private function exportEntryToPULSETRACKER($data, $form) {

        $form_id = $form['id'];
        $title = ( empty($form['title']) ? 'Gravity Form'. $form_id : $form['title'] );

        $credentials = get_option('gf_cloud_settings_userCredentials');
        $siteCode = trim(get_option('gf_cloud_settings_siteCode'));

        $strCURLOPT = ''; // $this->API_url.'/save/wpdata';
        
        $strCURLOPT .= 'API_Account=' . $credentials['API_Account'];
        $strCURLOPT .= '&API_Key=' . $credentials['API_Key'];
        $strCURLOPT .= '&Site_Code=' . $siteCode;
        $strCURLOPT .= '&Form_title=' . urlencode($title);
        $strCURLOPT .= '&Form_id=' . $form_id;
        $strCURLOPT .= '&Form_type=gravityforms';
        $strCURLOPT .= '&ip=' . urlencode(filter_input(INPUT_SERVER, 'REMOTE_ADDR'));
        $strCURLOPT .= '&random=' . time();

        $post = array('content' => base64_encode(serialize($data)));
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->API_url . '/import/gravity-form-data?' . ($strCURLOPT));

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
        return $output;
    }
}

PULSETRACKERGravityForm::getObject();

add_filter('gform_custom_merge_tags', 'pt_visitor_link_custom_merge_tags', 10, 4);
function pt_visitor_link_custom_merge_tags($merge_tags, $form_id, $fields, $element_id) {
    $merge_tags[] = array('label' => 'Visitor Link', 'tag' => '{visitor_link}');
    return $merge_tags;
}

add_filter( 'gform_notification', 'gform_notification_add_visitor_link', 10, 3 );
function gform_notification_add_visitor_link( $notification, $form, $entry ) {
    
    $pulsetrackerCustomFields = get_option('tft_gravity_form_' . $form['id']);
    $cid = filter_input(INPUT_COOKIE, 'tft_contact_id');
    $contact_id = (!empty($cid)) ? base64_decode(trim($cid)): 0;

    if(!empty($pulsetrackerCustomFields) && !empty($contact_id)){
        $visitor_link = 'Visitor Profile: '.PT_URL.'visitors/'.$contact_id.'/profile';
        $notification['message'] = str_replace('{visitor_link}', $visitor_link, $notification['message']);
    } else {
        $notification['message'] = str_replace('{visitor_link}', '', $notification['message']);
    }
    return $notification;
}