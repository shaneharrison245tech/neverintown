<div id="cf7MappingForm">
    <div id="gfcloud-formdata" class="gravity-form-m" style="display:block;">
        <input type="hidden" name="formId" value="<?php echo $formId;?>">
        <input type="button" name="map_button" id="map_button" value="Map Form Fields" style="padding:5px 10px 5px; cursor:pointer"> &nbsp; &nbsp; &nbsp; 
            
        <table id="gf_cloud_table" class="gf-table" width="100%" border="0">
            <tbody>
                <?php
                $i=1;
                foreach ($preview_code as $formObject) {
                    if ($formObject->type=="submit") {
                        continue;
                    }
                    ?>        
                    <tr id="row_<?php echo $formObject->name; ?>">
                        <td>
                            Contact Form 7 field:<br>
                            <input type="text" value="<?php echo $formObject->name; ?>" readonly="" name="cf7_form_source_<?php echo $i; ?>">
                        </td>
                        <td>
                            Select field to associate:<br>
                            <select id="select2" class="select2" name="custom_field_destination_<?php echo $i;?>">
                                <option value="">-- Unmapped --</option>
                                
                                <?php
                                    foreach ($customFields as $customFieldKey=>$customFieldLabel) { ?>
                                    <option value="<?php echo $customFieldKey;?>" <?php if ($mapFields['custom_field_destination_'.$i]==$customFieldKey) {?> selected <?php } ?>><?php echo $customFieldLabel; ?></option>
                                    <?php 
                                } ?>
                            </select>
                        </td>
                    </tr>
                    <?php
                    $i++;
                } ?>
            </tbody>
        </table>
    </div>
</div>

<div id="mappingModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Mapping your data</h4>
            </div>
            <div class="modal-body">
                <?php global $pt_data;?>
                <p><h5>Please wait, system is mapping fields with <?php echo $pt_data['Name']; ?>.</h5></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal.css" type="text/css" />
    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal-bs3patch.min.css" type="text/css" />

<style>
    
    .modal-dialog {
    position: relative;
    width: auto;
}
.modal-content {
    position: relative;
}
.modal-header {
    padding: 15px;
    border-bottom: 1px solid #e5e5e5;
}
.close {
    float: right;
    font-weight: 700;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #fff;
    filter: alpha(opacity=50);
    opacity: .5;
    margin-top: -7px;
    background-color: transparent;
    border: none;
    font-size: 30px;
    margin-right: -10px;
}
.modal-title {
    margin: 0;
    line-height: 1.42857143;
    font-size:14px;
}
.modal-body {
    position: relative;
    padding: 15px;
}
</style>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/js/bootstrap-modalmanager.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/js/bootstrap-modal.js"></script>

<script type="text/javascript">
    Array.prototype.inArray = function (value) {
        for (i = 0; i < this.length; i++) {
            if (this[i] === value)
                return true;
        }
        return false;
    }
    var title = "CF7_form_new";
    var form_id = '<?php echo $formId; ?>';
    jQuery(document).ready(function () {
        jQuery('#map_button').bind('click', save_list);
    });

    function save_list() {
        if (validateSelect()) {
            
            jQuery.fn.serializeObject = function()
            {
               var o = {};
               var a = this.serializeArray();
               jQuery.each(a, function() {
                   if (o[this.name]) {
                       if (!o[this.name].push) {
                           o[this.name] = [o[this.name]];
                       }
                       o[this.name].push(this.value || '');
                   } else {
                       o[this.name] = this.value || '';
                   }
               });
               return o;
            };
            var data = jQuery('#cf7MappingForm input').serializeObject();
            var data2 = jQuery('#cf7MappingForm select').serializeObject();
            
            jQuery.post(ajaxurl, {'param':data,'param2': data2, 'action': 'save_list_cf7'}).done(function () {
                jQuery("#mappingModal").modal('hide');
            });
        }
    }

    function validateSelect() {
        var data = document.getElementsByClassName('select2');
        var selectedArrays = Array();
        for (var i = data.length - 1; i >= 0; i--) {
            dataValue = data[i].options[data[i].selectedIndex].value;
            if (selectedArrays.inArray(dataValue)) {
                if (dataValue != '') {
                    alert("duplicate value " + (i + 1) + ' / ' + data[i].options[data[i].selectedIndex].text);
                    data[i].focus();
                    return false;
                }
            }

            selectedArrays.push(dataValue);
        }
        this.modalSuccess();
        return true;
    }

    function modalSuccess() {
        jQuery(document).ready(function ($) {
            jQuery("#mappingModal").modal('show');
        });
    }
</script>