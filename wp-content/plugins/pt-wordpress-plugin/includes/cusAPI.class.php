<?php

/*
  Description: The GF Integrations API Methods
  Author: Vimal Kumar
  Version: 1.0
 */

class gfComAPI_cloud {

    protected $v = '1.4.1';
    protected $enviroment = API_URL;
    protected $API_account = 'TFTa02b46cc132f1040732bca7ff57875649964b58d';
    protected $API_key = 'f4710c0e857bb6f4503cf4b95cd6904175692579';
   

    public function __construct() {
        $cUs_email = '';
        $cUs_formkey = '';
        return TRUE;
    }

    public function getAPICredentials($siteCode) {

        $ch = curl_init();

        $strCURLOPT = $this->enviroment.'/login';
        $strCURLOPT .= '?API_Account=' . $this->API_account;
        $strCURLOPT .= '&API_Key=' . $this->API_key;
        $strCURLOPT .= '&SiteCode=' . trim($siteCode);
	
    	// set url
    	curl_setopt($ch, CURLOPT_URL, $strCURLOPT);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	$output = curl_exec($ch);

    	curl_close($ch);
    	
    	return $output;
    }

    public function get_deeplink($forms) {

        // first check if is array of forms we are passing
        if (is_array($forms)) {

            foreach ($forms as $key => $value) {
                
                if (strlen($value->deep_link_view)) {
                    $deeplink = $value->deep_link_view;
                    break;
                }
            }
            
        } else {
            
            $deeplink = $forms; // this is just a string
        }

        $abtest = parse_url($deeplink);
        $link = $abtest['scheme'] . '://' . $abtest['host'] . $abtest['path'];
        return $link;
    }
    
    public function getDefaultFormID($cUsAPI_getFormKeys) {
        
        $cUs_jsonKeys = json_decode($cUsAPI_getFormKeys);
        
        if($cUs_jsonKeys->status == 'success' ){
        
            foreach ($cUs_jsonKeys->data as $oForms => $oForm) {
                if ( $oForm->form_type == 'post' && $oForm->default == 1 ){ //GET DEFAULT POST FORM KEY
                    $defaultFormId  = $oForm->form_id;
                }
            }
            
        }
        
        return $defaultFormId;
    }


    public function parse_deeplink($deeplink) {
        $abtest = parse_url($deeplink);
        $link = $abtest['scheme'] . '://' . $abtest['host'] . $abtest['path'];
        return $link;
    }

    public function getDefaultDeepLink($cUsAPI_getFormKeys) {

        if (empty($cUsAPI_getFormKeys))
            return FALSE;

        $cUs_jsonKeys = json_decode($cUsAPI_getFormKeys);

        if ($cUs_jsonKeys->status == 'success') {
            $deeplinkview = $this->get_deeplink($cUs_jsonKeys->data);
        }

        return $deeplinkview;
    }


    public function str_clean($str) {
        $str = str_replace("'", '', $str);
        $str = str_replace('\'', '', $str);

        return $str;
    }

 
    public function resetData() {
        delete_option('gf_cloud_settings');
        delete_option('gf_cloud_settings_userCredentials');

        return true;
    }

    public function _isCurl(){
        return function_exists('curl_version');
    }
}