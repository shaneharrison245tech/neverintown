<?php
//Plugin Name: Pulsetracker
//Description: This plugin integrates Pulsetracker with WordPress, Gravity Forms, and Contact Form 7.
//Author: Pulsetracker
//Version: 2.2.0

require_once "init.php";

add_filter( 'cron_schedules', 'pt_add_cron_interval_hourly' ); 
function pt_add_cron_interval_hourly( $schedules ) {
    $schedules['pt_hourly'] = array(
        'interval' => 10,
        'display'  => esc_html__( 'PT Every Hour' ),
    );
 
    return $schedules;
}

//check for update hourly a day (same schedule as normal WP plugins)
register_activation_hook(__FILE__, 'pt_check_activation');
function pt_check_activation() {
    wp_schedule_event(time(), 'pt_hourly', 'pt_check_event');
}

//remove cron task upon deactivation
register_deactivation_hook(__FILE__, 'pt_check_deactivation');
function pt_check_deactivation() {
    wp_clear_scheduled_hook('pt_check_event');
}
?>