<?php

// loginAlreadyUser handler function...
add_action('wp_ajax_gf_cloud_loginAlreadyUser', 'gfCloud_loginAlreadyUser_callback');

function gfCloud_loginAlreadyUser_callback() {

    $api = new gfComAPI_cloud();
    $siteCode = $_REQUEST['site_code'];
    
    try{
        $apiCredential = $api->getAPICredentials($siteCode); //api hook;
        $response = json_decode($apiCredential);
    } catch(Exception $e){
        $apiCredential = false;
    }
    if(empty($response)) $apiCredential = false;
    if($apiCredential) {
   
        if($response->success){
            $cUs_API_Account = $response->api_account;
            $cUs_API_Key = $response->api_key;
            $trackingScript = $response->trackingScript;
    
            if (strlen(trim($cUs_API_Account)) && strlen(trim($cUs_API_Key))) {
                $aryUserCredentials = array(
                    'API_Account' => $cUs_API_Account,
                    'API_Key' => $cUs_API_Key
                );
        
                update_option('gf_cloud_database_active', 1);
                update_option('gf_cloud_settings_userCredentials', $aryUserCredentials);
                update_option('gf_cloud_settings_siteCode', $siteCode);
                update_option('pt3_gf_tracking_script', $trackingScript);
                if(function_exists('curl_version')){
                    $aryResponse = array('status' => 1, 'message' => $response->message);    
                } else {
                    $aryResponse = array('status'=>3, 'message' => 'CURL NOT FOUND. Please ask your systems administrator to install curl on your server.');
                }
            }
        } else {
            $aryResponse = array('status' => 3, 'message' => $response->message);
        } 
        die(json_encode($aryResponse));
    }
    die(json_encode(array('status' => 3, 'message' => 'Could not authenticate.')));
}

add_action('wp_ajax_gf_cloud_logoutUser', 'gfCloud_logoutUser_callback');

function gfCloud_logoutUser_callback() {

    $cUsCF_api = new gfComAPI_cloud();
    $cUsCF_api->resetData(); //RESET DATA

    delete_option('gf_cloud_database_active');
    delete_option('gf_cloud_settings_userCredentials');
  
    echo 'Deleted.... User data'; //none list

    die();
}

function hook_tracking_script() {

	$output="<style> .wp_head_example { background-color : #f1f1f1; } </style>";

	echo $output;

}
/* end file ajax_response.php */