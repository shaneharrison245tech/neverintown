<?php
//change plugin name (white label)
	whitelabel();

	function whitelabel(){
		if(!is_admin()) return false;
		
		global $pt_data;
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		$pt_plugin = get_plugins('/' . plugin_basename( dirname( __FILE__ ) ));
		foreach ($pt_plugin as $_plugin) {
		    $pt_data = $_plugin;
		}

		delete_option('pt_plugin_title');
		delete_option('pt_plugin_icon');
		$siteCode = trim(get_option('gf_cloud_settings_siteCode'));
		$request = wp_remote_get(API_URL.'/pd/'.$siteCode);
		if( is_wp_error( $request ) ) { return false; }
		$body = wp_remote_retrieve_body( $request );
		$data = json_decode($body);

		if(!empty($data->name)) update_option('pt_plugin_title', $data->name);
		if(!empty($data->icon)) update_option('pt_plugin_icon', $data->icon);
		
		$pluginName = get_option('pt_plugin_title', 'Pulsetracker');
		if( $pt_data['Name'] != $pluginName ){
		    $content = file_get_contents(PT_PLUGIN_DIR.'/version.txt');
		    $content = str_replace('{title}', $pluginName, $content);
		    $content = str_replace('{version}', $pt_data["Version"], $content);
		    file_put_contents(PT_PLUGIN_DIR.'/index.php', $content);
		    $pt_data['Name'] = $pluginName;

		}
	}
//change plugin name (white label)

add_action('admin_head', 'pt_plugin_css');
function pt_plugin_css() {
  echo '<style type="text/css">
    #toplevel_page_gf-integrations .dashicons-before img{
		width: 19px;
		// height: 19px;
	}
  </style>';
}
?>