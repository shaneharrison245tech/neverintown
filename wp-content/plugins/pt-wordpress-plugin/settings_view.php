<?php
/**
 * File to display the view in GF
 * Company: Pulsetracker
 * */
 
// check if this plugin has been activated and validated with CU , otherwise don't show this error message
$gf_cloud_activated = get_option('gf_cloud_database_active');
$cf_enabled = is_plugin_active( 'contact-form-7/wp-contact-form-7.php' );
$gf_enabled = is_plugin_active( 'gravityforms/gravityforms.php' );
global $pt_data;
?>
<script>
    //<![CDATA[	
    var ADMIN_AJAX_URL = "<?php echo ADMIN_AJAX_URL; ?>";
    var template_url = "<?php echo get_bloginfo('template_url'); ?>";
//]]>
</script>
<!-- plugin admin header -->
<div class="wrap">
    <h1><?php echo $pt_data['Name']; ?> Integration</h1>
    
    <?php
    
    if (isset($_GET['reset']) && $_GET['reset']==true) {
	$redirectUrl =  admin_url('admin.php?page=gf-integrations');
      
	delete_option('gf_cloud_database_active');
	delete_option('gf_cloud_settings_userCredentials');
	delete_option('gf_cloud_settings_siteCode');
	delete_option('pt3_gf_tracking_script');
	print('<script>window.location.href="'.$redirectUrl.'"</script>');
    }
    
    
    // get option if plugin is already activated    
    $gfComAPI_cloud = new gfComAPI_cloud();
    if ($gf_cloud_activated == 1) {    
        // get the credentials
        $credentials = get_option('gf_cloud_settings_userCredentials');
        $cUs_API_Account    = $credentials['API_Account'];
        $cUs_API_Key        = $credentials['API_Key'];
        ?>
        <?php
    } ?>

	<!-- / plugin admin header -->
	<div id="container_gfclouddatabase">
		<!-- left side container -->
		<div id="CUintegrations_toleft">
			
			    <?php

			    if ($gf_cloud_activated) { ?>
			   
			    		<div class="welcome-panel">
			    			<div class="notice_visible welcome-panel-content"> 
			    				<h3>Thank you for using <?php echo $pt_data['Name'];?>!</h3>
								<p>Your site is currently being tracked.

								<?php
						    	if ( $gf_enabled ) { ?>
						    		<div style="float:left; width:50%;">
										<div class="welcome-panel">
											<div class="notice_visible welcome-panel-content"> 
												<h3>Gravity Forms Integration</h3>
												<p>To continue the setup process, follow these simple instructions.</p>

								    			<ol>
								    				<li>
								    					Go to the Gravity Forms admin panel and click on the form that you want to integrate. You can integrate an unlimited number of forms.
								    				</li>
								    				<li>
								    					At the top, click the Map Fields link to map your form fields to the fields in the database. If you have custom fields you will need to create them in <?php echo $pt_data['Name'];?> first.
							    					 </li>
								    				<li>
								    					After you click "Map Form Fields" a custom Lead list will be created and your leads will be automatically piped into <?php echo $pt_data['Name'];?>
								    				</li>
								    			</ol>
								    
												<input type="button" class="button button-primary button-hero button_redirect" name="buttoncontinue" id="buttoncontinue" value="Go to Gravity Forms >>" />
											</div>
										</div>
									</div>
									<?php
								}	?>
									
								<?php
								if ($cf_enabled) { ?>

									<div style="float:left; width:50%;">
										<div class="welcome-panel">
											<div class="notice_visible welcome-panel-content"> 
												<h3>Contact Form 7 Integration</h3>
												<p>To continue the setup process, follow these simple instructions.</p>

								    			<ol>
								    				<li>
								    					Go to the Contact Form 7 admin panel and click on the form that you want to integrate. You can integrate an unlimited number of forms.
								    				</li>
								    				<li>
								    					At the top, click the Map Fields tab to map your form fields to the fields in the database. If you have custom fields you will need to create them in <?php echo $pt_data['Name'];?> first.
							    					 </li>
								    				<li>
								    					After you click "Map Form Fields" a custom Lead list will be created and your leads will be automatically piped into <?php echo $pt_data['Name'];?>
								    				</li>
								    			</ol>
								    
												<input type="button" class="button button-primary button-hero button_redirect_cf7" name="buttoncontinue" id="buttoncontinue" value="Go to Contact Form 7 >>" />
											</div>
										</div>
									</div>
									<?php
								} ?>
							</div>
						</div>

				
						<div style="float:right; clear:both;">
							<a href="admin.php?page=gf-integrations&reset=true">Reset API Key</a>
						</div>
			<?php
		    } else { 
		    	?>
			<div class="stuffbox cf-login">
				<div class="inside">
					<div class="notice notice-error"><p></p></div>
					<!-- API KEY form -->
					<form method="post" action="admin.php?page=gf-integrations" id="gf_cloud_loginform" name="gf_cloud_loginform" class="steps" onsubmit="return false;">
						<fieldset>
							<legend class="edit-comment-author">Please enter the API key from your <?php echo $pt_data['Name']; ?> settings and click Connect.</legend>
							<table class="form-table editcomment">
								<tr>
								    <td class="first"><label for="site_code">API Key</label></td>
								    <td><input class="inputform" name="gf_cloud_settings[site_code]" id="site_code" type="text"></td>
								</tr>
								<tr>
								    <th></th>
								    <td>
									<input id="loginbtn" class="button button-primary gf_cloud_LoginUser" value="Connect" type="submit">
								    </td>
							    </tr>
							</table>
						</fieldset>
					</form>
				</div>
			</div>
			
		    <?php
		} ?>
		</div>
	</div>
</div>