<?php
$this_file = __FILE__;
$plugin_file = 'index.php';
$update_check = "https://pulsetracker.com/bundles/tftapp/downloads/version";

//Exclude from WP updates
function pt_updates_exclude( $r, $url ) {
	global $plugin_file;
	if ( 0 !== strpos( $url, 'http://api.wordpress.org/plugins/update-check' ) )
	return $r; // Not a plugin update request. Bail immediately.
	$plugins = json_decode( $r['body']['plugins'] );
	$plugins->plugins = (array) $plugins->plugins;
	unset( $plugins->plugins[ plugin_basename( dirname(__FILE__) ).$plugin_file ] );
	unset( $plugins->active[ array_search( plugin_basename( dirname(__FILE__) ).$plugin_file, $plugins->active ) ] );
	$r['body']['plugins'] = json_encode($plugins);
	return $r;
}

add_filter( 'http_request_args', 'pt_updates_exclude', 5, 2 );


//Returns current plugin info.
function pt_plugin_get($i) {
	global $this_file;
	global $plugin_file;
	if ( ! function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin_folder = get_plugins( '/' . plugin_basename( dirname( $this_file ) ) );
	return $plugin_folder[$plugin_file][$i];
}

add_action('pt_check_event', 'pt_check_update');
function pt_check_update() {
	global $wp_version;
	global $this_file;
	global $update_check;
	global $plugin_file;
	$plugin_folder = plugin_basename( dirname( $this_file ) );
	if ( defined( 'WP_INSTALLING' ) ) return false;

	$response = wp_remote_get( $update_check );
	list($version, $url) = explode('|', $response['body']);
	if(pt_plugin_get("Version") == $version) return false;
	$plugin_transient = get_site_transient('update_plugins');
	$a = array(
		'slug' => $plugin_folder,
		'new_version' => $version,
		'url' => pt_plugin_get("AuthorURI"),
		'package' => $url
	);
	$o = (object) $a;
	$plugin_transient->response[$plugin_folder.'/'.$plugin_file] = $o;
	set_site_transient('update_plugins', $plugin_transient);
}
//var_dump(wp_get_schedule('pt_check_update')); die;
?>